<?php


WE::import('lib.vendors.BdLibUcrypt');
WE::import('lib.WELibFunc');
WE::import('lib.vendors.BdBase64LibXor');
WE::import('lib.WELibHttpRequest');



class WELibPassport
{

    protected $options;


    private $_error = array();

    public static function getUserInfo($options)
    {
        $instance = new self($options);
        // 试图获取用户信息            
        $user_info = $instance->_getUserInfo();
        
        return $user_info;
    }

    private function __construct($options = null)
    {
        $this->options = array(
            'BDUSS' => false, // 自带BDUSS信息，如果设置则不从COOKIE中获取
            'ideabase64_encrypted' => false, // bduss是否被ideabase64加密过
            'bdbase64_encrypted' => false, //bduss是否用bdbase64加密过
            'bvs_ip_list' => array("10.36.7.65", "10.26.7.72", "10.81.211.104", "10.65.211.140"), // 认证接口ip
        );

        if ($options && is_array($options)) {
            $this->options = array_merge($this->options, $options);
        }

    }

    function __destruct()
    {
    }



    private function _getBDUSS()
    {
        if ($this->options['BDUSS']) {
            $bduss = $this->options['BDUSS'].'';  //此处的.''不能省略，否则就为地址copy，后边的ideabase64_decode函数会把原始数据破坏掉
        } else if (!empty($_COOKIE['BDUSS'])) {
            $bduss = $_COOKIE['BDUSS'];
        } else {
            return false;
        }

        if ($this->options['ideabase64_encrypted']) {
            $decodekey = ideabase64_decode($bduss);
            if ($decodekey === "0" || $decodekey === 0) {
                $length = unpack('V', substr($bduss, 0, 4));
                $tmp_arr = unpack('a'.$length[1], substr($bduss, 20, $length[1]));
                $bduss = $tmp_arr[1];
            } else {
                return false;
            }
        }else if($this->options['bdbase64_encrypted']){
        	$bduss = BdBase64LibXor::bd_base64_decode($bduss);
        }
        

        return $bduss;
    }

    private function _getUserInfo()
    {
        $bduss = $this->_getBDUSS();
        if (!$bduss) return false;
     
        if (__WE_ENV == 'LOCAL') {
            $ret = array(
                'uid' => '591619',
                'uname' => '_忘忧草_',
                'uc' => '03075fcdfcd3c7b2dd5f0900',
                'bduss' => $bduss,
            );
            return $ret;
        }

        $url = $this->_getAuthUrl($bduss);
        $response = WELibHttpRequest::simple_http_get($url, 1);
        $user_info = array();
        parse_str($response->body, $user_info);
        if (empty($user_info['uid'])) {
            return false;
        } else {
            if (empty($user_info['username'])) {
                $user_info['username'] = $user_info['uid'];
            }
            $struc = BdLibUcrypt::ucrypt_encode($user_info['uid'], $user_info['username']);
            $ret = array(
                'uid' => $user_info['uid'],
                'uname' => iconv("gbk", "utf-8", $user_info['username']),
                'uc' => $struc,
                'bduss' => $bduss,
            );
            return $ret;
        }
    }

    private function _getAuthUrl($bduss)
    {
        $random_int = rand(0, count($this->options['bvs_ip_list']) - 1);
        return "http://".$this->options['bvs_ip_list'][$random_int].":7801/ssn?apid=1075&cm=258&sid=$bduss&cip=".WELibFunc::getClientIp();
    }

  
}




