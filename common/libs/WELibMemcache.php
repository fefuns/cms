<?php

/**
  * WELibMemcache
  * @author 
  * 
  *  $cache = new WELibMemcache("twitter", array(WEConfCache, 'getMcacheServer')); // 创建一个cache实例
  *  $result = $cache->read(MY_KEY); // 读取MY_KEY的值，此时连接会自动建立和关闭
  *
  * 如果对cache需要有多次操作，建议将auto_close设置为false, 这样在操作的时候不会每次都建立和关闭连接
  * 但是注意一定要在操作完成之后主动调用close函数关闭！！！
  *  $cache = new WELibMemcache("twitter", array(WEConfCache, 'getMcacheServer'), false); // 创建一个cache实例，设置auto_close为false
  *  $result = $cache->read(MY_KEY1); // 读取
  *  $result = $cache->read(MY_KEY2);
  *  $result = $cache->write(MY_KEY1, MY_VALUE); // 设置
  *  ... ... ... ...
  *  $result = $cache->read(MY_KEYN);
  *  $cache->close(); // 关闭连接
  */
class WELibMemcache
{
    public static $keyPrefix = ''; // 键值的前缀，防止共用同一个实例时键值冲突

    protected $_tagname;
    protected $_host;
    protected $_conn = null;
    protected $_autoClose;

    /**
     * @param callable  $config_func  配置的回调函数，tagname传入后获取对应的Memcache服务器组
     * @param string  $tagname  配置标签
     * @param boolean $auto_close  是否自动关闭连接，true 由memcache类自动关闭连接，false 不会自动关闭连接
     * @throws WEException if the config is invalid
     */
    public function __construct($config_func, $tagname, $auto_close = true)
    {
        $this->_tagname = $tagname;
        $this->_configFunc = $config_func;
        $this->_autoClose = $auto_close;

        /**
         * 控制将key映射到server的策略
         * "consistent"可以允许在连接池中添加/删除服务器时不必重新计算 key与server之间的映射关系
         */
        ini_set('memcache.hash_strategy', 'consistent');
        /**
         * 控制将key映射到server的散列函数
         * 'fnv'则表示使用FNV-1a算法，NV-1a比CRC32速度稍低，但是散列效果更好
         */
        ini_set('memcache.hash_function', 'fnv');
    }

    public function __destruct()
    {
        $this->close();
    }

    /**
     * 依照构造函数传入的配置参数获取对应的Memcache服务器
     * @return array Memcache服务器信息
     * array(
     *   array('host'=>'', 'port'=>''),
     *   ...
     * )
     */
    protected function cacheServer()
    {
        if (!is_callable($this->_configFunc) || !$server = call_user_func($this->_configFunc, $this->_tagname)) {
            throw new WEException("Memcache config is invalid");
            return false;
        }
        return $server;
    }

    /**
     * 连接Memcache服务器
     */
    protected function connect()
    {
        if ($this->_conn) return $this->_conn; // 如果服务器没有关闭，直接返回

        $this->_host = $this->cacheServer();
        $this->_conn = new Memcache();
        foreach ($this->_host as &$host){
            $ret = $this->_conn->addServer($host['host'], $host['port'], false, 1, 3, -1);
            if($ret === false) {
                WELibLogger::rpc("{$this->_tagname}.connect Error", 5, $host['host'], $host['port'], WE_LOG_TYPE_MEMCACHE);
            }
        }
        $this->_conn->setCompressThreshold(20000, 0.2);
        return $this->_conn;
    }

    public function genCacheKey($key)
    {
        $key = self::$keyPrefix . $key;
        return $key;
    }

    /**
     * 关闭Memcache服务器
     * 本函数为选用，addServer会在连接完成后自动断开连接
     */
    public function close()
    {
        if ($this->_conn == null) return;
        @$this->_conn->close();
        $this->_conn = null;
    }

    /**
     * 读取Memcache中的数据
     * @param  mixed $key 缓存数据的键值，批量时可传入键值数组
     * @return mixed 缓存的数据，false表示错误或者key不存在
     */
    public function read($key)
    {
        if (!$key) {
            return false;
        }
        if (!is_array($key)) {
            $key = $this->genCacheKey($key);
        } else {
            foreach ($key as &$v) {
                $v = $this->genCacheKey($v);
            }
        }

        $_time_start = microtime(true);
        $this->connect();
        if ($this->_conn == null) return false;

        $ret = $this->_conn->get($key);
        if ($this->_autoClose) $this->close();

        $log_msg = $this->_tagname . '.read';
        if (is_array($key)) {
            $log_key = implode(',', $key);
        } else {
            $log_key = $key;
        }
        if( $ret === false ) {
            //WELibLogger::rpc("$log_msg Error #{$log_key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
            return false;
        }
        
        WELibLogger::rpc("$log_msg OK #{$log_key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
        return $ret;
    }

    /**
     * 往MEMCACHE中写入数据
     * @param  string $key 缓存数据的键值
     * @param  mixed  $val 要缓存的数据
     * @param  integer $expire  数据过期时间，0表示无限期。可以使用Unix时间戳，或者与当前时间相差的描述，后一种情况的秒数不能大于2592000(30天)
     * @return boolean 是否写入成功
     */
    public function write($key, $val, $expire = 0)
    {
        if (!$key) {
            return false;
        }
        $key = $this->genCacheKey($key);

        $_time_start = microtime(true);
        $this->connect();
        if ($this->_conn == null) return false;

        $ret = $this->_conn->set($key, $val, 0, $expire);
        if ($this->_autoClose) $this->close();

        $log_msg = $this->_tagname . '.write';
        if( $ret === false ) {
            WELibLogger::rpc("$log_msg Error #{$key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
            return false;
        }
        
        WELibLogger::rpc("$log_msg OK #{$key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
        return $ret;
    }

    /**
     * 增加元素的值
     * @param  string  $key   缓存数据的键值
     * @param  integer $value 增加的值
     * @return integer 增加后的新值
     */
    public function increment($key, $value=1)
    {
        if(!$key) {
            return false;
        }
        $key = $this->genCacheKey($key);

        $_time_start = microtime(true);
        $this->connect();
        if($this->_conn == null) return false;
        $ret = $this->_conn->increment($key, $value);
        if ($this->_autoClose) $this->close();

        $log_msg = $this->_tagname . '.increment';
        if( $ret === false ) {
            WELibLogger::rpc("$log_msg Error #{$key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
            return false;
        }
        
        WELibLogger::rpc("$log_msg OK #{$key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
        return $ret;
    }

    /**
     * 减少元素的值
     * @param  string  $key   缓存数据的键值
     * @param  integer $value 减去的值
     * @return integer 减去后的新值
     */
    public function decrement($key, $value=1)
    {
        if(!$key) {
            return false;
        }
        $key = $this->genCacheKey($key);

        $_time_start = microtime(true);
        $this->connect();
        if($this->_conn == null) return false;
        $ret = $this->_conn->decrement($key, $value);
        if ($this->_autoClose) $this->close();

        $log_msg = $this->_tagname . '.decrement';
        if( $ret === false ) {
            WELibLogger::rpc("$log_msg Error #{$key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
            return false;
        }
        
        WELibLogger::rpc("$log_msg OK #{$key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
        return $ret;
    }

    /**
     * 从Memcache中删除数据
     * @param  string  $key  缓存数据的键值
     * @return boolean 是否删除成功
     */
    public function delete($key)
    {
        if(!$key) {
            return false;
        }
        $key = $this->genCacheKey($key);

        $_time_start = microtime(true);
        $this->connect();
        if ($this->_conn == null) return false;
        $ret = $this->_conn->delete($key);
        if ($this->_autoClose) $this->close();

        $log_msg = $this->_tagname . '.delete';
        if( $ret === false ) {
            WELibLogger::rpc("$log_msg Error #{$key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
            return false;
        }
        
        WELibLogger::rpc("$log_msg OK #{$key}", microtime(true) - $_time_start, '', '', WE_LOG_TYPE_MEMCACHE);
        return true;
    }
}
?>
