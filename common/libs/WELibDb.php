<?php
/**
 * Handles all HTTP requests using cURL and manages the responses.
 *
 * @version 2013.08.01
 * 2013.08.01 by fandonghui 添加超时设置，抽取出简单的get与post的使用方法
 * @copyright 2006-2011 Ryan Parman
 * @copyright 2006-2010 Foleeo Inc.
 * @copyright 2010-2011 Amazon.com, Inc. or its affiliates.
 * @copyright 2008-2011 Contributors
 * @license http://opensource.org/licenses/bsd-license.php Simplified BSD License
 */
class WELibDb {
    
}