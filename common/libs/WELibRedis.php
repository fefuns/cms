<?php

WE::import('config.WEConfRedis');

/**
 * A wrapper of phpredis lib
 * @author 
 */
class WELibRedis
{
    private $_tagname;
    private $_config_func;
    private $_redisHost;
    private static $_redis = array();
    
    public function __construct($config_func, $tagname = '', $key = '')
    {
        $this->_config_func = $config_func;
        $this->_tagname = $tagname;
        $this->_key = $key;
    }
    
    /**
     * 依照服务号选择对应的Redis服务器
     * @return void
     */
    protected function redisServer()
    {
        if (!is_callable($this->_config_func) 
            || !$server = call_user_func_array($this->_config_func, array($this->_tagname, $this->_key)))
        {
            throw new WEException("Redis config is invalid");
            return false;
        }
        return $server;
    }
    
    /**
     * 连接Redis服务器
     * @return Object Redis
     */
    protected function connect()
    {
        $this->_redisHost = $this->redisServer();

        if(empty($this->_redisHost)){
            trigger_error("config [{$this->_const_server}] not existed", E_USER_ERROR);
            return;
        }
        $redisprint = "{redis}:host={$this->_redisHost['host']};port={$this->_redisHost['port']}";

        if(!array_key_exists($redisprint,self::$_redis)){
            $_time_start = microtime(true);

            try{
                $redis = new Redis();
                $redis->connect($this->_redisHost['host'],$this->_redisHost['port'],$this->_redisHost['timeout']);
                $redis->auth($this->_redisHost['auth']);
            } catch (Exception $e){
                WELibLogger::rpc("Error # Cannot connect to the Redis Server", microtime(true)-$_time_start, $this->_redisHost['host'], $this->_redisHost['port'], WE_LOG_TYPE_REDIS);
                throw new Exception("Redis system error");
            }
            
            self::$_redis[$redisprint] = $redis;
        }

        return self::$_redis[$redisprint];
    }

    /**
     * 通过方法重载将方法调用转发到Redis对象上
     * @param  string $method 方法名
     * @param  array  $args 传参
     * @return mixed
     */
    public function __call($method, $args)
    {
        $black_list = array('connect', 'open', 'pconnect', 'popen', 'auth');

        $redis = $this->connect();
        $callback = array($redis, $method);
        if (in_array($method, $black_list) || !is_callable($callback)) {
            trigger_error('call to unexisting method ' . $method, E_USER_ERROR);
            return false; 
        }

        $_time_start = microtime(true);
        $ret = call_user_func_array($callback, $args);
        WELibLogger::rpc("$method", microtime(true)-$_time_start, $this->_redisHost['host'], $this->_redisHost['port'], WE_LOG_TYPE_REDIS);

        return $ret;
    }
}