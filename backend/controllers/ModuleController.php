<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class ModuleController extends Controller
{
    public $layout = 'beyond';

    public function actionIndex()
    {
        // $this->params['breadcrumbs'][] = 'About Us';
        $connection = \Yii::$app->db;
        $command = $connection->createCommand('SELECT * FROM theme');
        $posts = $command->queryAll();
        // echo json_encode($posts);
        return $this->render('index');
    }

    
}
